import { Component, OnInit } from '@angular/core';
import { ApiATCService } from '../../services/api-atc.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  userId;
  rows;

  constructor(private authService: AuthService,
              private apiService: ApiATCService) {
    this.userId = this.authService.getAuthService();
    this.apiService.getWalletInfoByUser(this.userId)
      .subscribe( response => {
        this.rows = response;
      });
    }

  ngOnInit(): void {
  }

}
