import { Component, OnInit } from '@angular/core';
import { ColDef } from 'ag-grid-community';
import { Utils } from 'src/app/common/utils/utils-component';
import { ApiATCService } from 'src/app/services/api-atc.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {
  rows;
  transactions;
  symbol = '';
  paginationPageSize;
  rowSelection;
  rowGroupPanelShow;
  frameworkComponents;
  domLayout;
  private gridApi;
  private gridColumnApi;
  public rowClassRules;
  utils: Utils = new Utils();

  columnDefs: ColDef[] = [
    { headerName: 'Symbol', field: 'symbol', sortable: true, filter: true },
    { headerName: 'Quantity', field: 'qty', sortable: true, filter: true },
    {
      headerName: 'Buy Price',
      field: 'buyPrice',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.buyPrice, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Total Buy Price',
      field: 'totalBuyPrice',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.totalBuyPrice, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Actual Price',
      field: 'actualPrice',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.actualPrice, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Total Actual Price',
      field: 'totalActualPrice',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.totalActualPrice, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'P&L',
      field: 'pl',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.pl, '%'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    }
  ];

  columnDefsTr: ColDef[] = [
    { headerName: 'From', field: 'symbolFrom', sortable: true, filter: true },
    { headerName: 'To', field: 'symbolTo', sortable: true, filter: true },
    {
      headerName: 'Quantity',
      field: 'qty',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.qty, ''),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Buy Price',
      field: 'buyPrice',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.buyPrice, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Change/Sell Price',
      field: 'changePrice',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.changePrice, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Actual Price To',
      field: 'priceTo',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.priceTo, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Qty To',
      field: 'qtyTo',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.qtyTo, ''),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    }
  ];

  constructor(private apiService: ApiATCService) { }

  ngOnInit(): void {
    this.paginationPageSize = 10;
    this.rowSelection = 'multiple';
    this.rowGroupPanelShow = 'always';
    this.domLayout = 'autoHeight';

    this.rows = [];
    this.transactions = [];
  }

  startAnalytics() {
    this.apiService.getWalletInfoByUser(environment.userId/*, this.symbol*/)
      .subscribe( response => {
        this.rows = response;
      }
    );

    this.apiService.getTransactionByUser(environment.userId)
      .subscribe( response => {
        this.transactions = response;
      }
    );
  }
}
