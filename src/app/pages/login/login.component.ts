import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });
  error;
  constructor(private router: Router, private ngbActiveModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  signIn() {
    this.closeModal();
    this.router.navigateByUrl('/home');
  }

  closeModal() {
    this.ngbActiveModal.close();
  }


}
