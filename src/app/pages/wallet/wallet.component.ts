import { Component, OnInit, ViewChild } from '@angular/core';
import { ColDef } from 'ag-grid-community';
import { ApiATCService } from '../../services/api-atc.service';
import { AuthService } from '../../services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ButtonRendererComponent } from '../../common/cellRenderer/button-renderer/button-renderer.component';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexStroke,
  ApexGrid
} from 'ng-apexcharts';
import { AddAssetComponent } from 'src/app/common/add-asset/add-asset.component';
import { AddSellComponent } from 'src/app/common/add-sell/add-sell.component';
import { Utils } from 'src/app/common/utils/utils-component';
import { environment } from 'src/environments/environment';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  dataLabels: ApexDataLabels;
  grid: ApexGrid;
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
};

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})
export class WalletComponent implements OnInit {
  userId;
  rows;
  transactions;
  paginationPageSize;
  rowSelection;
  rowGroupPanelShow;
  frameworkComponents;
  domLayout;
  private gridApi;
  private gridColumnApi;
  public rowClassRules;
  public rowClassRulesTr;
  @ViewChild('chart') chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  prices: number[];
  labels: string[] = [];
  hoursPerDay = 24;
  utils: Utils = new Utils();

  columnDefs: ColDef[] = [
    { headerName: 'Symbol', field: 'symbol', sortable: true, filter: true },
    { headerName: 'Quantity', field: 'qty', sortable: true, filter: true },
    {
      headerName: 'Buy Price',
      field: 'buyPrice',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.buyPrice, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Total Buy Price',
      field: 'totalBuyPrice',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.totalBuyPrice, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Actual Price',
      field: 'actualPrice',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.actualPrice, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Total Actual Price',
      field: 'totalActualPrice',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.totalActualPrice, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'P&L',
      field: 'pl',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.pl, '%'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Sell',
      cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        onClick: this.onSellClick.bind(this),
        label: 'sell'
      },
      cellStyle: { 'justify-content': 'center' }
    },
    {
      headerName: 'View Detail',
      cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        onClick: this.onViewDetail.bind(this),
        label: 'info'
      },
      cellStyle: { 'textAlign': 'center' }
    }
  ];

  columnDefsTr: ColDef[] = [
    { headerName: 'From', field: 'symbolFrom', sortable: true, filter: true },
    { headerName: 'To', field: 'symbolTo', sortable: true, filter: true },
    {
      headerName: 'Quantity',
      field: 'qty',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.qty, ''),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Buy Price',
      field: 'buyPrice',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.buyPrice, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Change/Sell Price',
      field: 'changePrice',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.changePrice, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Actual Price To',
      field: 'priceTo',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.priceTo, '€'),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Qty To',
      field: 'qtyTo',
      sortable: true,
      valueFormatter: params => this.utils.currencyFormatter(params.data.qtyTo, ''),
      filter: 'agNumberColumnFilter',
      filterParams: {
        suppressAndOrCondition: true
      }
    },
    {
      headerName: 'Associated Rule',
      cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        onClick: this.onAssociatedRule.bind(this),
        label: 'rule'
      },
      cellStyle: { 'textAlign': 'center' }
    }
  ];

  constructor(private apiService: ApiATCService,
              private modalService: NgbModal) {
    this.userId = environment.userId;

    this.apiService.getWalletInfoByUser(this.userId)
      .subscribe( response => {
        this.rows = response;

        this.apiService.getValueUserWallet(response)
        .subscribe( innerResponse => {
          this.prices = innerResponse;
          this.chartOptions = {
            series: [
              {
                name: 'Price',
                data: this.prices,
                color: '#002b5c'
              }
            ],
            chart: {
              height: 350,
              type: 'line',
              zoom: {
                enabled: true
              }
            },
            dataLabels: {
              enabled: false
            },
            stroke: {
              curve: 'straight'
            },
            grid: {
              row: {
                colors: ['#f3f3f3', 'transparent'],
                opacity: 0.5
              }
            },
            xaxis: {
              categories: this.labels,
              floating: true
            }
          };
        });

      }
    );

    this.apiService.getLabels()
      .subscribe( response => {
        this.labels = response;
    });

    this.apiService.getTransactionByUser(this.userId)
      .subscribe( response => {
        this.transactions = response;
      });

    this.frameworkComponents = {
      buttonRenderer: ButtonRendererComponent,
    };

    this.rowClassRules = {
      'gain-pl': 'data.pl >= 0',
      'loss-pl': 'data.pl < 0',
    };

    this.rowClassRulesTr = {
      'loss-pl': 'data.buyPrice * data.qty >= data.priceTo * data.qtyTo',
      'gain-pl': 'data.buyPrice * data.qty < data.priceTo * data.qtyTo',
    };

  }

  ngOnInit(): void {
    this.paginationPageSize = 10;
    this.rowSelection = 'multiple';
    this.rowGroupPanelShow = 'always';
    this.domLayout = 'autoHeight';
  }

  openAddAsset() {
    const modalRef = this.modalService.open(AddAssetComponent, {
       centered: true,
       backdropClass: 'light-blue-backdrop'
      });

    modalRef.componentInstance.idWallet = this.transactions[0].walletId;
  }

  openAddTransaction() {
    const modalRef = this.modalService.open(AddSellComponent, {
      centered: true,
      backdropClass: 'light-blue-backdrop'
     });

    modalRef.componentInstance.idWallet = this.transactions[0].walletId;
  }

  onSellClick(e) {
    const modalRef = this.modalService.open(AddSellComponent, {
      centered: true,
      backdropClass: 'light-blue-backdrop'
     });

    modalRef.componentInstance.idWallet = this.transactions[0].walletId;
    modalRef.componentInstance.symbolFromValue = e.rowData.symbol;
    modalRef.componentInstance.buyPriceValue = e.rowData.buyPrice;
    modalRef.componentInstance.qtyValue = e.rowData.qty;
  }

  onAssociatedRule(e) {
    console.log(e.rowData);
  }

  onViewDetail(e) {
    console.log(e.rowData);
  }

}
