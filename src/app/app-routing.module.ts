import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './common/page-not-found/page-not-found.component';
import { AnalyticsComponent } from './pages/analytics/analytics.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { LoginComponent } from './pages/login/login.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';
import { TermsComponent } from './pages/terms/terms.component';
import { WalletComponent } from './pages/wallet/wallet.component';


const routes: Routes = [
  {path: 'home', component: HomepageComponent},
  {path: 'analytics', component: AnalyticsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'wallet', component: WalletComponent},
  {path: 'terms', component: TermsComponent},
  {path: 'privacy', component: PrivacyComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
