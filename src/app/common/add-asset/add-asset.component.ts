import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { toJSDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-calendar';
import { WalletDetail } from 'src/app/model/model';
import { ApiATCService } from 'src/app/services/api-atc.service';

@Component({
  selector: 'app-add-asset',
  templateUrl: './add-asset.component.html',
  styleUrls: ['./add-asset.component.css']
})
export class AddAssetComponent implements OnInit {
  origins = ['Binance', 'Kraken', 'Coinbase', 'Others'];
  idWallet;
  addAssetForm: FormGroup;

  constructor(private ngbActiveModal: NgbActiveModal,
              private formBuilder: FormBuilder,
              private atcService: ApiATCService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.addAssetForm = this.formBuilder.group({
      symbol: new FormControl(),
      origin: new FormControl([Validators.required]),
      qty: new FormControl([Validators.required]),
      price: new FormControl([Validators.required]),
    });
  }


  get symbol(): any {
    return this.addAssetForm.get('symbol');
  }

  get origin(): any {
    return this.addAssetForm.get('origin');
  }

  get qty(): any {
    return this.addAssetForm.get('qty');
  }

  get price(): any {
    return this.addAssetForm.get('price');
  }

  closeModal() {
    this.ngbActiveModal.close();
  }

  onSubmit() {
    let walletDetail = new WalletDetail(this.idWallet, this.addAssetForm.value.symbol,
      this.addAssetForm.value.origin, this.addAssetForm.value.qty, this.addAssetForm.value.price);

    this.atcService.insertWalletDetail(walletDetail)
      .subscribe( response => {
        if (response === 1) {
          this.closeModal();
          this.openSnackBar('Asset inserted Correctly', ':)');
        } else if (response === 0) {
          this.openSnackBar('ERRORS during asset insertion', ':(');
        }
      });
  }

  openSnackBar(message: string, action: string) {
    if (action === ':)') {
      this.snackBar.open(message, action, {
        duration: 4000,
        panelClass: ['green-snackbar']
      });
    } else if (action === ':())') {
      this.snackBar.open(message, action, {
        duration: 5000,
        panelClass: ['red-snackbar']
      });
    }
  }

}
