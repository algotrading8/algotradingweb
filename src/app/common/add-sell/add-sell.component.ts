import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { WalletTransaction } from 'src/app/model/model';
import { ApiATCService } from 'src/app/services/api-atc.service';

@Component({
  selector: 'app-add-sell',
  templateUrl: './add-sell.component.html',
  styleUrls: ['./add-sell.component.css']
})
export class AddSellComponent implements OnInit {
  idWallet;
  symbolFromValue;
  buyPriceValue;
  qtyValue;
  addSellForm: FormGroup;

  constructor(private ngbActiveModal: NgbActiveModal,
              private formBuilder: FormBuilder,
              private atcService: ApiATCService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.addSellForm = this.formBuilder.group({
      symbolFrom: new FormControl(),
      symbolTo: new FormControl(),
      buyPrice: new FormControl([Validators.required]),
      changePrice: new FormControl([Validators.required]),
      qty: new FormControl([Validators.required]),
      qtyTo: new FormControl([Validators.required]),
      priceTo: new FormControl([Validators.required]),
    });
  }

  closeModal() {
    this.ngbActiveModal.close();
  }

  onSubmit() {
    let walletDetail = new WalletTransaction(this.idWallet, this.addSellForm.value.symbolFrom,
      this.addSellForm.value.symbolTo, this.addSellForm.value.buyPrice, this.addSellForm.value.changePrice,
      this.addSellForm.value.qty, this.addSellForm.value.priceTo, this.addSellForm.value.qtyTo
      );

    this.atcService.insertWalletTransaction(walletDetail)
      .subscribe( response => {
        if (response === 1) {
          this.closeModal();
          this.openSnackBar('Transaction inserted Correctly', ':)');
        } else if (response === 0) {
          this.openSnackBar('ERRORS during transaction insertion', ':(');
        }
      });
  }

  openSnackBar(message: string, action: string) {
    if (action === ':)') {
      this.snackBar.open(message, action, {
        duration: 4000,
        panelClass: ['green-snackbar']
      });
    } else if (action === ':())') {
      this.snackBar.open(message, action, {
        duration: 5000,
        panelClass: ['red-snackbar']
      });
    }
  }
}
