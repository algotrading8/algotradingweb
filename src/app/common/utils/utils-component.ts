export class Utils {
    currencyFormatter(currency, sign) {
        const sansDec = currency.toFixed(2);
        const formatted = sansDec.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        return `${formatted}` + sign;
      }

}
