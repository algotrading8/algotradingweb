import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WalletDetail, WalletTransaction } from '../model/model';

@Injectable({
    providedIn: 'root'
  })
export class ApiATCService {
    url = 'http://localhost:8083/';

    constructor(public http: HttpClient) {
    }

    getWalletInfoByUser(userID: string): Observable<any> {
        return this.http.post(this.url + 'userwallet/value/', userID);
    }

    getTransactionByUser(userID: string): Observable<any> {
        return this.http.post(this.url + 'userwallet/transaction/', userID);
    }

    getLabels(): Observable<any> {
        return this.http.get(this.url + 'userwallet/chartLabel');
    }

    getValueUserWallet(wallet: any): Observable<any> {
        return this.http.post(this.url + 'userwallet/chartValue/', wallet);
    }

    insertWalletDetail(walletDetail: WalletDetail): Observable<any> {
        return this.http.post(this.url + 'userwallet/insertWalletDetail', walletDetail);
    }

    insertWalletTransaction(walletTransaction: WalletTransaction): Observable<any> {
        return this.http.post(this.url + 'userwallet/insertWalletTransaction', walletTransaction);
    }

}
