export interface UserLogin {
    userName: string;
    userPwd: string;
}

export class WalletDetail {
    detailId: number;
    walletId: string;
    symbol: string;
    originSource: string;
    qty: number;
    buyPrice: number;

    constructor(wallet: string, symbol: string, origin: string,
                qty: number, price: number) {
        this.detailId = 0;
        this.walletId = wallet;
        this.symbol = symbol;
        this.originSource = origin;
        this.qty = qty;
        this.buyPrice = price;
    }
}

export class WalletTransaction {
    transactionId: number;
    walletId: string;
    symbolFrom: string;
    symbolTo: string;
    buyPrice: number;
    changePrice: number;
    qty: number;
    priceTo: number;
    qtyTo: number;

    constructor(wallet: string, symbolFrom: string, symbolTo: string,
                buyPrice: number, changePrice: number, qty: number,
                priceTo: number, qtyTo: number) {
        this.transactionId = 0;
        this.walletId = wallet;
        this.symbolFrom = symbolFrom;
        this.symbolTo = symbolTo;
        this.buyPrice = buyPrice;
        this.changePrice = changePrice;
        this.qty = qty;
        this.priceTo = priceTo;
        this.qtyTo = qtyTo;
    }
}
