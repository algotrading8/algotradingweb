import { Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';
import { delay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './pages/login/login.component';
import { environment } from 'src/environments/environment';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'AlgoTradingWeb';
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;
  showSideBar = false;

  constructor(private router: Router,
              private authService: AuthService,
              private observer: BreakpointObserver,
              private modalService: NgbModal) {}

  ngOnInit(): void {
   this.router.navigateByUrl('/analytics');
   environment.userId = this.authService.getAuthService();
   /*this.modalService.open(LoginComponent,
    {
      size: 'l',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });*/
  }

  ngAfterViewInit() {
    this.observer
      .observe(['(max-width: 800px)'])
      .pipe(delay(1))
      .subscribe((res) => {
        if (res.matches) {
          this.sidenav.mode = 'over';
          this.sidenav.close();
        } else {
          this.sidenav.mode = 'side';
          this.sidenav.open();
        }
      });
  }

  onClick(selectedButton): void {
    if (selectedButton === 'profile') {
      this.router.navigateByUrl('/login');
    }
  }
}
